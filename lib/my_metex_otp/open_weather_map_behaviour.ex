defmodule MyMetexOtp.OpenWeatherMapBehaviour do
  @moduledoc """
  Behaviours associated with the [Open Weather Web Service Client API](https://openweathermap.org/).

  See also: `MyMetexOtp.OpenWeatherMapClient`
  """

  @callback get(String.t()) :: {:ok, %HTTPoison.Response{}} | {:error, %HTTPoison.Error{}}
end
