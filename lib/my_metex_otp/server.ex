defmodule MyMetexOtp.Server do
  @moduledoc false

  use GenServer

  alias MyMetexOtp.WeatherService

  ## delegated calls

  def start_service do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def get_temperature(location) when is_binary(location) do
    GenServer.call(__MODULE__, {:location, location})
  end

  def get_stats do
    GenServer.call(__MODULE__, :get_stats)
  end

  def reset_stats do
    GenServer.cast(__MODULE__, :reset_stats)
  end

  def stop_service do
    GenServer.cast(__MODULE__, :stop)
  end

  ## Server Callbacks

  def init(:ok) do
    {:ok, %{}}
  end

  def handle_call({:location, location}, _from, stats) do
    case WeatherService.temperature_of(location) do
      {:ok, temp} ->
        # update the frequency of the location
        new_stats = WeatherService.update_stats(stats, location)
        {:reply, "#{temp}°C", new_stats}

      _error ->
        {:reply, :error, stats}
    end
  end

  def handle_call(:get_stats, _from, stats) do
    {:reply, stats, stats}
  end

  def handle_cast(:reset_stats, _stats) do
    {:noreply, %{}}
  end

  def handle_cast(:stop, stats) do
    {:stop, :normal, stats}
  end

  # Called by Genserver just before server termination.
  # Params:
  #  reason: reason why the server terminated; usually :normal (from handle_cast/2 :stop above)
  #  stats:  current state
  def terminate(reason, stats) do
    # We could write to a file, database etc.
    IO.puts("server terminated because of #{inspect(reason)}. stats: #{inspect(stats)}")

    # must always return :ok
    :ok
  end

  # Called by GenServer for messages not handled by either handle_call/3 and handle_cast/2 (out-of-band messages).
  # Example
  #  iex> {:ok, pid} = MyMetexOtp.start_service
  #  iex> send pid, "It's raining kitties!"          # 'send(pid)' is an unknown function to GenServer
  #  received "It's raining men"
  def handle_info(msg, stats) do
    IO.puts("received #{inspect(msg)}")
    {:noreply, stats}
  end
end
