[![pipeline status](https://gitlab.com/adequateDeveloper/my_metex_otp/badges/master/pipeline.svg)](https://gitlab.com/adequateDeveloper/my_metex_otp/commits/master)

# MyMetexOtp - The weather report from around the world


An updated version of the application as described in Chapter 4 of [The Little Elixir & OTP Guidebook](https://www.manning.com/books/the-little-elixir-and-otp-guidebook).

## Design
The design implements the concepts as taught in the [Elixir for Programmers](https://pragdave.me/) course by PragDave.

Namely the public API layer is separated in its own top-level module from its associated and underlying OTP server module, which is also separated from the business logic layer module.

* Public API - MyMetexOtp
* GenServer - MyMetexOtp.Server
* Business Logic - MyMetexOtp.WeatherService

The application utilizes the [Open Weather Web Service API](https://openweathermap.org/). As such, the design wraps this 3rd party dependency in such a way as to enable unit testing.

## 3rd Party Dependency Adapter

Review the relationships between the following:
* WeatherService - OpenWeatherMapClient
* OpenWeatherMapClient - config.exs

## Mox-based Testing
Review project notes for Mox-based testing resources.

Review the following files:
* open_weather_map_client.ex - for its @callback behaviour
* test_helper.exs - for its OpenWeatherMapMock declaration
* config/*.exs - for the open_weather_map_adapter: declarations
* open_weather_map_client_test.exs - for the Mox test implementation

## Top-level Dependencies

* httpoison
* json
* mox
* ex_doc
* doctor
* dialyxir

## Project Notes
Review the ./.gitlab-ci.yml file
